let adminUser = localStorage.getItem("isAdmin");
// fetch
fetch(url + '/api/admin')
.then( res => res.json())
.then(data => {
	// console.log(data) //array
	let coursesContainer = document.querySelector('#coursesContainer');
	let cardFooter = (course) => {
		if (adminUser === "false" || !adminUser) {
			return `<a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">Select Course</a>`
		} else if( course.isActive == true ) {		
			return `
				<a href="./addCourse.html?courseId=${course._id}" class="btn btn-info text-white btn-block editButton" >Add Course</a>
				<a href="./studentDetails.html?courseId=${course._id}" class="btn btn-warning text-white btn-block editButton" >Students List</a>
				<a href="./editCourse.html?courseId=${course._id}" class="btn btn-primary text-white btn-block editButton" >Edit</a>
				<a href="./deleteCourse.html?courseId=${course._id}" class="btn btn-danger text-white btn-block dangerButton">Disable course</a>
			`
		} else {
			return `
			<a href="./addCourse.html?courseId=${course._id}" class="btn btn-info text-white btn-block editButton" >Add Course</a>
			<a href="./studentDetails.html?courseId=${course._id}" class="btn btn-warning text-white btn-block editButton" >Students List</a>
			<a href="./editCourse.html?courseId=${course._id}" class="btn btn-primary text-white btn-block editButton" >Edit</a>
			<a href="./activateCourse.html?courseId=${course._id}" class="btn btn-success text-white btn-block dangerButton">Activate Course</a>`
		}
	}
	let courseCard = (params) => (`
		<div class="col-md-6 my-3">
			<div class="card">
				<div class="card-body">
					<h5 class="card-title">${params.name}</h5>
					<p class="card-text text-left">
						${params.description}
					</p>
					<div class="card-text text-right">
						&#8369; ${params.price}
					</div>
				</div>
				<div class="card-footer">
					${cardFooter(params)}
				</div>
			</div>
		</div>
	`)
	let courseData;
	if (data.length < 1){
		courseData = "No courses available";
	} else {
		courseData = data.map( course => {
			return courseCard(course);
		}).join("")
	}

	coursesContainer.innerHTML = courseData;
})
