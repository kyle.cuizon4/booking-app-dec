let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId');


let courseName = document.querySelector('#courseName');
let courseDesc = document.querySelector('#courseDesc');
let coursePrice  = document.querySelector('#coursePrice');
let enrollContainer = document.querySelector('#enrollContainer');

fetch(`${url}/api/courses/${courseId}`)
.then( res => res.json())
.then( data => {

	console.log(data)
	courseName.innerHTML = data.name
	courseDesc.innerHTML = data.description
	coursePrice.innerHTML = data.price
	enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-primary">Enroll</button>`
	
	let enrollButton = document.querySelector('#enrollButton')
	enrollButton.addEventListener('click', () =>{

		fetch(`${url}/api/users/enroll`, {
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json',
				'Authorization' : localStorage.getItem('token')
			},
			body : JSON.stringify({ courseId })
		})
		.then( res => res.json())
		.then( data => {
			console.log(data)
			if (data === true) {
				alert('Thank you for enrolling! See you!')
				window.location.replace('./courses.html')
			} else {
				alert("Something went wrong.")
			}
		})

	})
})


